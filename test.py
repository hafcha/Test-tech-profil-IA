import logging
from functools import wraps
from time import ctime, sleep
from logging.handlers import RotatingFileHandler

################################################
"""


"""
import logging
from functools import wraps
from logging.handlers import RotatingFileHandler

def log_content(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        # création de l'objet logger qui va nous servir à écrire dans les logs
        logger = logging.getLogger(func.__name__)
        # on met le niveau du logger à DEBUG, comme ça il écrit tout
        logger.setLevel(logging.DEBUG)
        # création d'un formateur qui va ajouter le temps, le niveau
        # de chaque message quand on écrira un message dans le log
        formatter = logging.Formatter('[%(asctime)s][file:%(filename)s]%(levelname)s [@%(name)s] variable ')
        
        # création d'un handler qui va rediriger une écriture du log vers
        # un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
        file_handler = RotatingFileHandler('logfile.log', 'a', 1000000, 1)
        # on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
        # créé précédement et on ajoute ce handler au logger
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        
        # création d'un second handler qui va rediriger chaque écriture de log
        # sur la console
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.DEBUG)
        logger.addHandler(stream_handler)
        
        logger.debug('hi')
        return result

    return wrapper
        


@log_content
def example(a, b, c):
    a = b + c
    b = a + c
    c = a + b
    d = "hi there"
    result = a + b + c
    return result

# Driver Code 
if __name__ == '__main__':
    example(1,2,3)
